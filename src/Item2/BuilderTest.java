package Item2;

public class BuilderTest {
    public static void main(String[] args) {
        BuilderTest();
    }

    private static void BuilderTest(){
        NutritionFacts cocaCola = new NutritionFacts.Builder(240,8)
                .calories(100).sodium(35).carbohydrate(27).build();

        System.out.println(cocaCola);
    }
}
